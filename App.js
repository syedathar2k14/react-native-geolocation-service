import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  PermissionsAndroid
} from 'react-native';
import MapView,{PROVIDER_GOOGLE} from 'react-native-maps'
import Geolocation from 'react-native-geolocation-service'
console.disableYellowBox = true;



class App extends Component {

constructor(props){
  super(props);
  this.state={
    region:{
      latitude: 24.9163102,
      longitude: 67.1302234,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }
  }
}

  async componentWillMount() {
    const granted = await this.requestLocationPermission()

    if (granted) {
      Geolocation.getCurrentPosition(
          (position) => {
              console.log(position);
          },
          (error) => {
              // See error code charts below.
              console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
  }
  }

  async requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Example App',
        'message': 'Example App access to your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the location")
      alert("You can use the location");
      return granted;
    } else {
      console.log("location permission denied")
      alert("Location permission denied");
      return granted;
    }
  } catch (err) {
    console.warn(err)
  }
 
}


render() {
  return (
    <View style={{flex:1, backgroundColor:'red'}}>
      <MapView 
      style={{flex:1, justifyContent:'center', alignContent:'center', }}
      provider={PROVIDER_GOOGLE}
      initialRegion={this.state.region}
      />
    </View>

  );
}
};


export default App;
